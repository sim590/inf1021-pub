/* Jeremie Gobeil <jeremie.gobeil@gmail.com> */

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <errno.h>

#define ADDRESS "127.0.0.1"
#define PORT 8000
#define BUFFERSIZE 1024

int main(int argc, char** argv) {
    printf("Démmarage du client udp en c++");
    printf("Création du fichier socket\n");
    int sockfile = socket(AF_INET, SOCK_DGRAM, 0);
    printf("Numéro du descripteur de fichier du socket: %i\n", sockfile);

    printf("Connecter le socket l'adresse locale %s:%i\n", ADDRESS, PORT);
    struct in_addr inp;
    inet_aton(ADDRESS, &inp);
    struct sockaddr_in addr = {
        AF_INET,  // sa_family_t    sin_family; /* address family: AF_INET */
        PORT,     // in_port_t      sin_port;   /* port in network byte order */
        inp       // struct in_addr sin_addr;   /* internet address */
    };
    if (connect(sockfile, (struct sockaddr*)&addr, sizeof(addr)) == -1){
        int errsv = errno;
        printf("Erreur lors de la connection\n");
        printf("%s\n", strerror(errsv));
        return errsv;
    }

    printf("Le client envoie un message sur le socket.\n");
    char buff[BUFFERSIZE];
    strcpy(buff, "Alloo");
    ssize_t sendsize = send(sockfile, (void*)&buff, BUFFERSIZE, 0);
    printf("Message envoyé: %s\n", buff);
    printf("Bytes transmis: %li\n", sendsize);

    return 0;
}
