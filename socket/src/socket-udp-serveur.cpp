/* Jeremie Gobeil <jeremie.gobeil@gmail.com> */

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <arpa/inet.h>

#define ADDRESS "0.0.0.0"
#define PORT 8000
#define BUFFERSIZE 1024

int main(int argc, char** argv) {
    printf("Démmarage du serveur udp en c++");
    printf("Création du fichier socket\n");
    int sockfile = socket(AF_INET, SOCK_DGRAM, 0);
    printf("Numéro du descripteur de fichier du socket: %i\n", sockfile);

    printf("Lier le socket à l'adresse locale %s:%i\n", ADDRESS, PORT);
    struct in_addr inp;
    inet_aton(ADDRESS, &inp);
    struct sockaddr_in addr = {
        AF_INET,  // sa_family_t    sin_family; /* address family: AF_INET */
        PORT,     // in_port_t      sin_port;   /* port in network byte order */
        inp       // struct in_addr sin_addr;   /* internet address */
    };
    // On doit utiliser un cast pour que le compilateur ne donne pas d'erreur.
    // Permet d'utiliser la même fonction pour plusieurs structures d'adresse.
    int binderr = bind(sockfile, (struct sockaddr*)&addr, sizeof(addr));
    if (binderr != 0){
        printf("Erreur lors de la liaison\n");
        return binderr;
    }

    printf("Le serveur attends un message sur le socket (bloquant).\n");
    char buff[BUFFERSIZE];
    recv(sockfile, (void*)&buff, BUFFERSIZE, 0);

    printf("Message reçu: %s\n", buff);


    return 0;
}

