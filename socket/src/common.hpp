/* Jeremie Gobeil <jeremie.gobeil@gmail.com> */

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

#define IP_SERVEUR "0.0.0.0"
#define IP_CLIENT "127.0.0.1"
#define PORT_UDP 8000
#define PORT_TCP 8001
#define BUFFERSIZE 1024000
#define WAITQUEUE 12

#define err_check(x) if((x) == -1){\
    int errsv = errno;\
    printf("%s\n", strerror(errsv));\
    return -1;}

