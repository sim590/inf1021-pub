/* Jeremie Gobeil <jeremie.gobeil@gmail.com> */

#include "common.hpp"

int main(int argc, char** argv) {
    // Variables
    int sock;                    // Numéro de fichier du socket en écoute
    struct sockaddr_in addr;     // Adresse du socket
    char buff[BUFFERSIZE];       // Buffer de communication
    ssize_t bytessend;           // Nombre de bytes transférés
    ssize_t bytesrecv;           // Nombre de bytes reçu

    int consock;                 // Numéro de fichier du socket de la connexion entrante
    struct sockaddr_in conaddr;  // Adresse du socket pour la connexion entrante
    socklen_t conaddr_len;       // Taille de la structure pour la connexion entrante

    printf("Serveur socket de type TCP en C/C++\n");

    printf("Création du socket\n");
    err_check( sock = socket(AF_INET, SOCK_STREAM, 0) );
    printf("Socket créer. Numéro du descripteur de fichier: %i\n", sock);

    printf("Création de la structure d'adresse pour %s:%i\n", IP_SERVEUR, PORT_TCP);
    addr.sin_family = AF_INET;                                 /* address family: AF_INET */
    addr.sin_port  = PORT_TCP;                                 /* port in network byte order */
    err_check( inet_aton(IP_SERVEUR, &addr.sin_addr) );        /* internet address */

    printf("Liaison de l'adresse local au socket\n");
    // On doit utiliser un cast pour que le compilateur ne donne pas d'erreur.
    err_check( bind(sock, (struct sockaddr*)&addr, sizeof(addr)) );

    printf("Mettre le socket en mode écoute avec une liste d'attente de %i\n", WAITQUEUE);
    err_check( listen(sock, WAITQUEUE) );

    printf("Serveur en attente d'une demande de connection\n");
    err_check( consock = accept(sock, (struct sockaddr*)&conaddr, &conaddr_len) );

    printf("Connexion reçu de %s:%i\n", inet_ntoa(conaddr.sin_addr), conaddr.sin_port);

    printf("réception d'un message\n");
    err_check( bytesrecv = recv(consock, (void*)buff, BUFFERSIZE, 0) );
    printf("%li bytes reçu\n", bytesrecv);

    printf("Réception d'un message\n");
    err_check( bytesrecv = recv(consock, (void*)buff, BUFFERSIZE, 0) );
    printf("%li bytes reçu\n", bytesrecv);

    printf("Envoie d'un message\n");
    strcpy(buff, "Alloo");
    err_check( bytessend = send(consock, (void*)buff, 6, 0) );
    printf("%li bytes envoyés\n", bytessend);

    printf("Fermeture des sockets\n");
    err_check( close(sock) );
    err_check( close(consock) );

    return 0;
}

