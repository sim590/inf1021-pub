/* Jeremie Gobeil <jeremie.gobeil@gmail.com> */

#include "common.hpp"

int main(int argc, char** argv) {
    // Variables
    int sock;                    // Numéro de fichier du socket en écoute
    struct sockaddr_in addr;     // Adresse du socket
    char buff[BUFFERSIZE];       // Buffer de communication
    ssize_t bytessend;           // Nombre de bytes transférés
    ssize_t bytesrecv;           // Nombre de bytes reçu

    printf("Client socket de type TCP en C/C++\n");

    printf("Création du socket\n");
    err_check( sock = socket(AF_INET, SOCK_STREAM, 0) );
    printf("Socket créer. Numéro du descripteur de fichier: %i\n", sock);

    printf("Création de la structure d'adresse pour %s:%i\n", IP_CLIENT, PORT_TCP);
    addr.sin_family = AF_INET;                                 /* address family: AF_INET */
    addr.sin_port  = PORT_TCP;                                 /* port in network byte order */
    err_check( inet_aton(IP_CLIENT, &addr.sin_addr) );        /* internet address */

    printf("Connection au serveur\n");
    // On doit utiliser un cast pour que le compilateur ne donne pas d'erreur.
    err_check( connect(sock, (struct sockaddr*)&addr, sizeof(addr)) );

    printf("Envoie d'un message\n");
    strcpy(buff, "Alloo");
    err_check( bytessend = send(sock, (void*)buff, BUFFERSIZE, 0) );
    printf("%li bytes envoyés\n", bytessend);

    printf("Réception d'un message\n");
    err_check( bytesrecv = recv(sock, (void*)buff, BUFFERSIZE, 0) );
    printf("%li bytes reçu\n", bytesrecv);

    printf("Fermeture du socket\n");
    err_check( close(sock) );

    return 0;
}
